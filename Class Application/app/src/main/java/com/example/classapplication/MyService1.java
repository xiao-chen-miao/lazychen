package com.example.main2;

import android.app.Service;
import android.content.Intent;
import android.media.MediaParser;
import android.media.MediaPlayer;
import android.os.IBinder;
import android.util.Log;

public class MyService1 extends Service {
    MediaPlayer mp;

    @Override
    public void onDestroy() {
        mp.stop();
        mp.release();

        super.onDestroy();
        Log.d("cjj","MyService:OnDestroy....");
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        Log.d("cjj","MyService:OnStart....");
        mp.start();
        return super.onStartCommand(intent, flags, startId);

    }

    @Override
    public void onCreate() {
        super.onCreate();
        mp = MediaPlayer.create(getApplicationContext(),R.raw.music);

        Log.d("cjj","MyService:OnCreate....");

    }

    public MyService1() {
    }

    @Override
    public IBinder onBind(Intent intent) {
        // TODO: Return the communication channel to the service.
        throw new UnsupportedOperationException("Not yet implemented");
    }
}
package com.example.main2;

import androidx.activity.result.ActivityResult;
import androidx.activity.result.ActivityResultCallback;
import androidx.activity.result.ActivityResultLauncher;
import androidx.activity.result.contract.ActivityResultContracts;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;

public class Activity1 extends AppCompatActivity {

    Button button1;
    Button button2;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_1);

        button1 = findViewById(R.id.button1);
        button2 = findViewById(R.id.button2);

        Log.d("cjj", "Activity1:onCreate");
        Context context = this;

        // 调用registerForActivityResult()方法来注册一个对Activity结果的监听。
        // 对传回的ResultCode进行判断
        ActivityResultLauncher launcher1 = registerForActivityResult(
                new ActivityResultContracts.StartActivityForResult(),
                new ActivityResultCallback<ActivityResult>() {
                    @Override
                    public void onActivityResult(ActivityResult result) {
                        if(result.getResultCode() == RESULT_OK){
                            Log.d("cjj", "onActivityResult:data=" + result.getData().getStringExtra("data_return"));
                        }
                    }
                }
        );

        button1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view){

                // 从A1通信到A2
                Intent intent1 = new Intent(Activity1.this, Activity2.class);

                // 新建一个bundle对象，将数据存入bundle中，然后向A2中传输bundle
                Bundle bundle = new Bundle();
                bundle.putString("name","cjj");
                bundle.putInt("age",20);
                intent1.putExtras(bundle);
                intent1.putExtra("name","chenjunjie");
                intent1.putExtra("age",20);

                startActivity(intent1);
            }
        });


        button2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // 从A1通信到A3
                Intent intent2 = new Intent(Activity1.this, Activity3.class);
                intent2.putExtra("name","cjj");
//                startActivityForResult(intent, 1);

                launcher1.launch(intent2);
            }
        });

    }

    @Override
    protected void onStop() {
        super.onStop();
        Log.d("cjj", "Activity1:onStop");
    }

    @Override
    protected void onStart() {
        super.onStart();
        Log.d("cjj", "Activity1:onStart");
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        Log.d("cjj", "Activity1:onDestroy");
    }

    @Override
    protected void onPostResume() {
        super.onPostResume();
        Log.d("cjj", "Activity1:onPostResume");
    }

}
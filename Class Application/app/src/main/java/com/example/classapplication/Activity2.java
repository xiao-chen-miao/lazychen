package com.example.main2;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

public class Activity2 extends AppCompatActivity {

    TextView textView;

    @SuppressLint("SetTextI18n")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_2);

        textView = findViewById(R.id.textView_a2);
        Log.d("cjj", "Activity2:onCreate");

        Intent intent2 = getIntent();
        intent2.getIntExtra("age",1);
//        textView.setText(intent2.getStringExtra("name"));

        // 获取A1传输的数据
        Bundle bundle = intent2.getExtras();
        textView.setText(bundle.getString("name") + bundle.getInt("age"));
    }

    @Override
    protected void onStop() {
        super.onStop();
        Log.d("cjj", "Activity2:onStop");
    }

    @Override
    protected void onStart() {
        super.onStart();
        Log.d("cjj", "Activity2:onStart");
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        Log.d("cjj", "Activity2:onDestroy");
    }

    @Override
    protected void onPostResume() {
        super.onPostResume();
        Log.d("cjj", "Activity2:onPostResume");

    }

}
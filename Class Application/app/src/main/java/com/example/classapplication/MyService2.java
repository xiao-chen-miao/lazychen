package com.example.main2;

import android.app.Service;
import android.content.Intent;
import android.media.MediaPlayer;
import android.os.Binder;
import android.os.IBinder;
import android.util.Log;

public class MyService2 extends Service {
    public MyService2() {
    }

    MediaPlayer mp;
    @Override
    public void onDestroy() {
//        mp.stop();
        mp.release();
        super.onDestroy();
        Log.d("cjj","MyService2:OnDestroy....");
    }

    @Override
    public void onCreate() {
        super.onCreate();

        Log.d("cjj","MyService2:OnCreate....");

    }
    @Override
    public IBinder onBind(Intent intent) {
        // TODO: Return the communication channel to the service.

        MyBinder binder = new MyBinder();
        return binder;
    }
    public class MyBinder extends Binder{
        public MyBinder(){
            super();
        }
        public void MusicPlay(){
            Log.d("cjj","MyService2:MusicPlay....");
            mp = MediaPlayer.create(getApplicationContext(),R.raw.music);
            mp.start();

        }
    }
}

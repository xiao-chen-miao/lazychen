package com.example.main2;

import
        androidx.appcompat.app.AppCompatActivity;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.Bundle;
import android.os.IBinder;
import android.view.View;
import android.widget.Button;


public class MusicActivity1 extends AppCompatActivity {

    Button button1,button2,button3,button4;
    MyService2.MyBinder myBinder;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_music1);

        button1 = findViewById(R.id.button);
        button2 = findViewById(R.id.button4);
        button3 = findViewById(R.id.button7);
        button4 = findViewById(R.id.button8);



        Intent intent1 = new Intent(MusicActivity1.this, MyService1.class);

        button1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startService(intent1);
            }
        });
        button2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                stopService(intent1);
            }
        });

        ServiceConnection connection = new ServiceConnection() {
            @Override
            public void onServiceConnected(ComponentName componentName, IBinder iBinder) {
                myBinder = (MyService2.MyBinder) iBinder;
                myBinder.MusicPlay();
            }

            @Override
            public void onServiceDisconnected(ComponentName componentName) {

                myBinder = null;
            }
        };

        button3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent3 = new Intent(MusicActivity1.this, MyService2.class);
               bindService(intent3,connection, Context.BIND_AUTO_CREATE);
                //startService(intent3);
            }
        });

        button4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                unbindService(connection);
            }
        });

    }
}
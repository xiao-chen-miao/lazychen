package com.example.main2;



import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

public class Activity3 extends AppCompatActivity {

    Button button3;
    TextView textView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_3);
        Log.d("cjj", "Activity3:onCreate");

        button3=findViewById(R.id.button3);
        textView=findViewById(R.id.textView_a3);

        // 获取通信,将文本设置成传输过来的数据
        Intent intent2 = getIntent();
        textView.setText(intent2.getStringExtra("name"));
        button3.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View view) {
                Log.d("cjj", "onClick:");
                Intent intent=new Intent();

                intent.putExtra("data_return", "返回的数据");
                // 返回一个 ActivityResultLauncher 对象，说明这是A3传输的数据
                setResult(RESULT_OK, intent);
                finish();
            }
        });
    }

    @Override
    protected void onStop() {
        super.onStop();
        Log.d("cjj", "Activity3:onStop");
    }

    @Override
    protected void onStart() {
        super.onStart();
        Log.d("cjj", "Activity3:onStart");
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        Log.d("cjj", "Activity3:onDestroy");
    }

    @Override
    protected void onPostResume() {
        super.onPostResume();
        Log.d("cjj", "Activity3:onPostResume");

    }

}
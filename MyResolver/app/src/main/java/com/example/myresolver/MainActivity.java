package com.example.myresolver;

import androidx.appcompat.app.AppCompatActivity;

import android.annotation.SuppressLint;
import android.content.ContentResolver;
import android.content.ContentValues;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    private ContentResolver resolver;
    private static final String AUTHORITY = "cjj.Provider1";
    private static final Uri NOTIFY_URI = Uri.parse("content://" + AUTHORITY+ "/person");
    Uri uri = Uri.parse("content://cjj.Provider1/person");
    Button button1,button2,button3, button4;
    TextView textView;

    ContentValues contentValues1, contentValues2, contentValues3;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        button1 = findViewById(R.id.button1);
        button2 = findViewById(R.id.button2);
        button3 = findViewById(R.id.button3);
        button4 = findViewById(R.id.button4);
        textView = findViewById(R.id.textView);

        // provider的Authority地址
        Uri uri1 = NOTIFY_URI;
        Uri uri2 = Uri.parse("content://sms");

        resolver = getContentResolver();



        contentValues1 =  new ContentValues();
        contentValues2 =  new ContentValues();
        contentValues3 =  new ContentValues();


        button1.setOnClickListener(new View.OnClickListener() {
            // 增
            @Override
            public void onClick(View view) {
                contentValues1.put("name", "cjj");
                contentValues1.put("age", 20);
                contentValues2.put("name", "wk");
                contentValues2.put("age", 21);
                contentValues3.put("name", "wk");
                contentValues3.put("age", 99);
                resolver.insert(NOTIFY_URI, contentValues1);
                resolver.insert(NOTIFY_URI, contentValues2);
                resolver.insert(NOTIFY_URI, contentValues3);
            }
        });

        button2.setOnClickListener(new View.OnClickListener() {
            // 删
            @Override
            public void onClick(View view) {
                int x = resolver.delete(NOTIFY_URI, "name = 'cjj'", null);

                textView.setText(x + "行被改变");
            }
        });

        button3.setOnClickListener(new View.OnClickListener() {
            // 改
            @Override
            public void onClick(View v) {

                contentValues2.put("name", "wk");
                contentValues2.put("age", 21);
                int x = resolver.update(NOTIFY_URI, contentValues2,"name = 'wk'", null);

                textView.setText(x + "行被改变");
            }
        });

        button4.setOnClickListener(new View.OnClickListener() {
            // 查
            @Override

            public void onClick(View v) {
                Cursor c = resolver.query(NOTIFY_URI, null, "name = 'wk'", null, null);

                c.moveToFirst();
                @SuppressLint("Range")int id = c.getInt(c.getColumnIndex("id"));
                @SuppressLint("Range")String name = c.getString(c.getColumnIndex("name"));
                @SuppressLint("Range")int age = c.getInt(c.getColumnIndex("age"));


                textView.setText("id:" + id +", name:" + name + ", age:" + age);
            }
        });
    }

}
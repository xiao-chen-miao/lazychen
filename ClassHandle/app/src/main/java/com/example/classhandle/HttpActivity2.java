package com.example.myapplicationhandle1;

import androidx.appcompat.app.AppCompatActivity;


import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.VideoView;

import com.bumptech.glide.Glide;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

public class HttpActivity2 extends AppCompatActivity {
    TextView textView1;
    Button button1, button2;
    ImageView imageView1;
    VideoView videoView;
    String result;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_http2);
        textView1 = findViewById(R.id.textView);
        button1 = findViewById(R.id.httpbutton); 
        button2 = findViewById(R.id.httpbutton2); 
        imageView1 = findViewById(R.id.imageView);
        videoView = findViewById(R.id.videoView);

    
        button1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new MyText().execute(); 
                new MyImg().execute(); 
                new MyStreamMedia().execute(); 
            }
        });


       
        button2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                okHttpText(); 
                GlideImg(); 
                okHttpStreamMedia();
            }
        });
    }

   
    class MyText extends AsyncTask<String, String, String> {
        @Override
        protected String doInBackground(String... strings) {
            try {
                
                URL url = new URL("https://www.baidu.com");
                
                HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();
                
                InputStream inputStream = urlConnection.getInputStream();
                InputStreamReader inputStreamReader = new InputStreamReader(inputStream);
                BufferedReader reader = new BufferedReader(inputStreamReader);
                Log.d("kai:原生方式", reader.toString());
                result = reader.readLine();
            } catch (MalformedURLException e) {
                Log.e("kai", e.toString());
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
                Log.e("kai", e.toString());
            }
            return result;
        }


        @Override
        protected void onPostExecute(String o) {
            textView1.setText(o);
        }
    }

    class MyImg extends AsyncTask<String, String, Bitmap> {
        @Override
        protected Bitmap doInBackground(String... strings) {
            try {
                URL url = new URL("https://httpbin.org/image/png");
                HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();
                InputStream inputStream = urlConnection.getInputStream();
                return BitmapFactory.decodeStream(inputStream);
            } catch (MalformedURLException e) {
                Log.e("kai", e.toString());
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
                Log.e("kai", e.toString());
            }
            return null;
        }

        @Override
        protected void onPostExecute(Bitmap bitmap) {
            if (bitmap != null) {
                imageView1.setImageBitmap(bitmap);
            }
        }
    }


    class MyStreamMedia extends AsyncTask<String, Void, Uri> {

        @Override
        protected Uri doInBackground(String... strings) {
            Uri uri = null;
            try {
                URL url = new URL("http://www.w3school.com.cn/i/movie.mp4");
                uri = Uri.parse(url.toString());
            } catch (MalformedURLException e) {
                Log.e("kai", e.toString());
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
                Log.e("kai", e.toString());
            }
            return uri;
        }

        @Override
        protected void onPostExecute(Uri uri) {
            if (uri != null) {
                videoView.setVideoURI(uri);
                videoView.start();
            }
        }
    }

    public void okHttpText() {
        OkHttpClient okHttpClient = new OkHttpClient();
        MediaType mediaType = MediaType.parse("application/json; charset=utf-8");
        RequestBody requestBody = RequestBody.create(mediaType, "{}");
        // 创建Request对象，设置URL和请求方法为POST，并将RequestBody对象传入
        Request request = new Request.Builder()
                .url("https://www.httpbin.org/post") // 请求的URL
                .post(requestBody)
                .build();

        // 异步请求
        okHttpClient.newCall(request).enqueue(new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
                Log.d("kai", "Error");
            }

            @Override
            public void onResponse(Call call, Response response) throws IOException {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        textView1.setText(response.body().toString());
                    }
                });
            }
        });
    }

    public void GlideImg() {
        // 使用Glide库加载图片
        Glide.with(getApplicationContext())
                .load("https://httpbin.org/image/png")
                .into(imageView1);
    }

    public void okHttpStreamMedia() {
        OkHttpClient okHttpClient = new OkHttpClient();

        Request request = new Request.Builder()
                .url("http://www.w3school.com.cn/i/movie.mp4") // 请求的URL
                .build();

        okHttpClient.newCall(request).enqueue(new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
                Log.d("kai", "Error");
            }

            @Override
            public void onResponse(Call call, Response response) throws IOException {
                InputStream inputStream = response.body().byteStream();

                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        try {
                            File tempFile = File.createTempFile("video", ".mp4", getCacheDir());
                            FileOutputStream fileOutputStream = new FileOutputStream(tempFile);

                            byte[] buffer = new byte[4096];
                            int bytesRead;
                            while ((bytesRead = inputStream.read(buffer)) != -1) {
                                fileOutputStream.write(buffer, 0, bytesRead);
                            }

                            // 关闭流
                            fileOutputStream.close();
                            inputStream.close();

                            videoView.setVideoPath(tempFile.getAbsolutePath());
                            videoView.start();
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }
                });
            }
        });
    }
}
//    public void testGet1() {
//        // 创建实例对象
//        OkHttpClient okHttpClient = new OkHttpClient();
//        // 创建Request对象
//        Request request = new Request.Builder()
////                .url("https://www.httpbin/org/get?id=111")
//                .url("https://www.baidu.com")
//                .addHeader("key", "value")
//                .get()
//                .build();
//        // 执行Request请求
//        // 异步请求
//        okHttpClient.newCall(request).enqueue(new Callback() {
//            @Override
//            public void onFailure(Call call, IOException e) {
//                Log.d("kai", "Error");
//            }
//
//            @Override
//            public void onResponse(Call call, Response response) throws IOException {
//                Log.d("kai", response.body().toString());
//            }
//        });
//    }
//
//    public void testPost1() {
//        OkHttpClient okHttpClient = new OkHttpClient();
//        //2. 创建Request对象
//        MediaType mediaType = MediaType.parse("application/json; charset=utf-8");
//        RequestBody requestBody = RequestBody.create(mediaType, "{}");
//        Request request = new Request.Builder()
//                .url("https://www.httpbin.org/post")
//                .post(requestBody)
//                .build();
//        // 异步请求
//        okHttpClient.newCall(request).enqueue(new Callback() {
//            @Override
//            public void onFailure(Call call, IOException e) {
//                Log.d("kai", "Error");
//            }
//
//            @Override
//            public void onResponse(Call call, Response response) throws IOException {
//                Log.d("kai", response.body().toString());
//            }
//        });
//    }
//
//    public void testPng() {
//        // 1. 创建实例对象
//        OkHttpClient okHttpClient = new OkHttpClient();
//        // 2. 创建Request对象
//        MediaType mediaType = MediaType.parse("image/png; charset=utf-8");
//        RequestBody requestBody = RequestBody.create(mediaType, "{}");
//        Request request = new Request.Builder()
//                .url("https://httpbin.org/image/png")
////                .post(requestBody)
//                .get()
//                .build();
//        // 异步请求
//        okHttpClient.newCall(request).enqueue(new Callback() {
//            @Override
//            public void onFailure(Call call, IOException e) {
//                // 请求失败
//                Log.d("kai", "Error");
//            }
//
//            @Override
//            public void onResponse(Call call, Response response) throws IOException {
////                Log.d("kai", response.body().toString());
//                Log.d("kai", response.body().toString());
//                Bitmap bitmap = BitmapFactory.decodeStream(response.body().byteStream());
//                // 返回给主线程信息
//                runOnUiThread(new Runnable() {
//                    @Override
//                    public void run() {
//                        imageView1.setImageBitmap(bitmap);
//                    }
//                });
//            }
//        });
//    }
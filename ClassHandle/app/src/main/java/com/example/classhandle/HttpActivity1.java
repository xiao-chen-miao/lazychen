package com.example.myapplicationhandle1;

import androidx.appcompat.app.AppCompatActivity;
import androidx.loader.content.AsyncTaskLoader;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

public class HttpActivity1 extends AppCompatActivity {
    TextView textView1;
    Button button1, button2, button3;
    ImageView imageView1;
    String result;

    // 一个异步任务，使用原生方法
    class MyTask extends AsyncTask<String, String, String> {
        @Override
        protected String doInBackground(String... strings) {
            try {
                URL url = new URL("https://www.baidu.com");
                HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();
                InputStream inputStream = urlConnection.getInputStream();
                InputStreamReader inputStreamReader = new InputStreamReader(inputStream);
                // 原生方法解析图片
//                Bitmap bitmap = BitmapFactory.decodeStream(inputStream);
//                imageView1.setImageBitmap(bitmap);

                BufferedReader reader = new BufferedReader(inputStreamReader);
                Log.d("kai:原生方式", reader.toString());
                result = reader.readLine();
            } catch (MalformedURLException e) {
//                throw new RuntimeException(e);
                Log.e("kai", e.toString());
                e.printStackTrace();
            } catch (IOException e) {
//                throw new RuntimeException(e);
                e.printStackTrace();
                Log.e("kai", e.toString());
            }
            return result;
//            return null;
        }

        // 收到的值能够放在UI主线程中显示
        @Override
        protected void onPostExecute(String o) { // 将收到的信息放在主线程中显示出来
//            super.onPostExecute(o);
//            o = "abc";
            textView1.setText(o);
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_http1);
        textView1 = findViewById(R.id.textView);
        button1 = findViewById(R.id.httpbutton);
        button2 = findViewById(R.id.httpbutton2);
        button3 = findViewById(R.id.httpbutton3);
        imageView1 = findViewById(R.id.imageView);

        // 原生
        button1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new MyTask().execute();
            }
        });

        // 第三方库Glide，适合做图片
        button2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Glide.with(getApplicationContext())
                        .load("https://httpbin.org/image/png")
                        .into(imageView1);
            }
        });

        // 第三方库okhttp3
        button3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                testGet1();
                testPost1();
                testPng();
            }
        });
    }

    public void testGet1() {
        // 创建实例对象
        OkHttpClient okHttpClient = new OkHttpClient();
        // 创建Request对象
        Request request = new Request.Builder()
//                .url("https://www.httpbin/org/get?id=111")
                .url("https://www.baidu.com")
                .addHeader("key", "value")
                .get()
                .build();
        // 执行Request请求
        // 异步请求
        okHttpClient.newCall(request).enqueue(new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
                Log.d("kai", "Error");

            }

            @Override
            public void onResponse(Call call, Response response) throws IOException {
                Log.d("kai", response.body().toString());
            }
        });
        // 同步方法
//        try {
//            Response response = okHttpClient.newCall(request).execute();
//            Log.d("kai", response.body().toString());
//        } catch (IOException e) {
//            Log.d("kai", "Error");
//            throw new RuntimeException(e);
//        }
    }

    public void testPost1() {
        OkHttpClient okHttpClient = new OkHttpClient();
        //2. 创建Request对象
//        String str1 = "100";
//        Integer.parseInt(str1);
        MediaType mediaType = MediaType.parse("application/json; charset=utf-8");
        RequestBody requestBody = RequestBody.create(mediaType, "{}");
        Request request = new Request.Builder()
                .url("https://www.httpbin.org/post")
                .post(requestBody)
                .build();
        // 同步
//        try {
//            Response response = okHttpClient.newCall(request).execute();
//            Log.d("kai", response.body().toString());
//        } catch (IOException e) {
//            Log.d("kai", "Error");
//            throw new RuntimeException(e);
//        }
        // 异步请求
        okHttpClient.newCall(request).enqueue(new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
                Log.d("kai", "Error");
            }

            @Override
            public void onResponse(Call call, Response response) throws IOException {
                Log.d("kai", response.body().toString());
            }
        });
    }

    public void testPng() {
        // 1. 创建实例对象
        OkHttpClient okHttpClient = new OkHttpClient();
        // 2. 创建Request对象
        MediaType mediaType = MediaType.parse("image/png; charset=utf-8");
        RequestBody requestBody = RequestBody.create(mediaType, "{}");
        Request request = new Request.Builder()
                .url("https://httpbin.org/image/png")
//                .post(requestBody)
                .get()
                .build();
        // 异步请求
        okHttpClient.newCall(request).enqueue(new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
                // 请求失败
                Log.d("kai", "Error");
            }

            @Override
            public void onResponse(Call call, Response response) throws IOException {
//                Log.d("kai", response.body().toString());
                Log.d("kai", response.body().toString());
                Bitmap bitmap = BitmapFactory.decodeStream(response.body().byteStream());
                // 返回给主线程信息
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        imageView1.setImageBitmap(bitmap);
                    }
                });
            }
        });
    }
}
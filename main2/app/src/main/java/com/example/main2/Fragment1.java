package com.example.main2;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.main2.databinding.Fragment1Binding;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.List;


public class Fragment1 extends Fragment {

    private Fragment1Binding binding1;
    RecyclerView recyclerView;
    List<Figures> list;
    List<Integer> imgList;
    Context context;
    MyAdapter myadapter;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
//        View view=inflater.inflate(R.layout.fragment1,container,false);
        binding1 = Fragment1Binding.inflate(inflater,container,false);
        View view = binding1.getRoot();
        context = view.getContext();


        recyclerView=binding1.recyclerView1;
        list = new ArrayList<>();
        imgList = new ArrayList<Integer>();
        initData();
//        initImage();

        // LinearLayoutManager用作列表布局,
        LinearLayoutManager manager=new LinearLayoutManager(context);
        recyclerView.setLayoutManager(manager);
//        设置网格布局
//        recyclerView.setLayoutManager(new GridLayoutManager(context,2));
//         设置垂直布局（默认垂直）
        manager.setOrientation(LinearLayoutManager.VERTICAL);
//        安卓提供了默认的item删除动画
        recyclerView.setItemAnimator(new DefaultItemAnimator());

//      DividerItemDecoration的实现类用于实现分割线
//        我们需要自己实现ItemDecoration接口
        DividerItemDecoration dividerItemDecoration = new DividerItemDecoration(context,DividerItemDecoration.VERTICAL);
        recyclerView.addItemDecoration(dividerItemDecoration);

// 调用适配器
        myadapter = new MyAdapter(list,imgList, context);
        recyclerView.setAdapter(myadapter);

        return view;
    }

    public void onDestroyView(){
        super.onDestroyView();
        binding1 = null;
    }

    private void initData() {
        list.add(new Figures("1","相生祐子", "----地表最强人类", "留着茶色短发，性格活泼开朗，有时候很有干劲，倒霉时也容易被哄开心。对生活有乐观的憧憬，对朋友很是珍惜。"));
        list.add(new Figures("2","长野原美绪", "----地表最强兵器", "留着勿忘草色的双孖辫，几乎每次都会扎着木立方体的头饰。经常会做出一些惊人的举动。"));
        list.add(new Figures("3","水上麻衣", "----麻衣大神", "是沉默的优等生，标准三无、黑长直、眼镜娘。戴着眼镜，留着黑色长发，眼睛只睁开一半。平日总是带着一些秘密。"));
        list.add(new Figures("4","博士", "----年仅6岁的天才博士", "留着橙色的长发，除了睡觉之外每天穿着白大褂。8岁的天才萝莉科学家，自称博士。对研究有着由衷的感情，成天闭门不出只为研究。"));
//        list.add(new Figures("东云名乃", "----博士制造的机器人", "女高中生机器人，留着黑色短发，实际年龄1岁。对博士可谓是无微不至的关怀。十项全能，让博士等人大开眼界，十分渴望成为一名普通的女生。"));
//        list.add(new Figures("阪本先生", "----“请叫我先生”", "被博士偶然捡到的小黑猫。"));
//        list.add(new Figures("中之条刚", "----倒霉的中之条", "留着庞克莫西干头。外表为典型的不良少年，但行为非常乖巧诚实。不相信非科学现象。"));
//        list.add(new Figures("立花美里", "----傲娇的军火姬", "个性傲娇。虽然参加了剑道社，但总是使用枪之类的武器狠狠的射穿笹原的头，攻击性很强。会随着傲娇程度增加而拿出更强的武器。"));
//        list.add(new Figures("笹原幸治郎", "----农家长男罢了", "演剧社社长。乘着白色山羊（名称为笹原小次郎）上学，有留着巴哈发型的随从跟着，说话很像贵族，但实际上只是农家长男。"));
//        list.add(new Figures("东云校长", "----秃头校长", "时定高中校长，62岁，外表是随处可见的校长，但学生和老师常因为他奇特的言行感到困惑。在给学生们讲话时喜欢讲一些过时的笑话，导致现场迅速冷场."));
//        list.add(new Figures("长野原佳乃", "----剑道天才", "美绪的姐姐，大学生。曾为时定高中的剑道社前辈，是个能在全国大赛出场并取得优胜的传奇人物。"));
//        list.add(new Figures("日常", "京都动画改编的动画"));
    }

//    public void initImage(){
//        Field[] fields = R.drawable.class.getFields();
//        for (Field field : fields) {
//            try {
//                if(field.getType() == int.class) {
//                    String fieldName = field.getName();
//                    if(fieldName.contains("figure")){
//                        int resId = field.getInt(null);
//                        imgList.add(resId);
//                    }
//                }
//            } catch (IllegalAccessException e) {
//                throw new RuntimeException(e);
//            }
//        }
//    }

    public class Figures {

        private String id;
        private String name;
        private String stalk;

        private String introduction;

        public Figures(String id,String name,String stalk,String introduction) {
            this.id = id;
            this.name = name;
            this.stalk = stalk;
            this.introduction = introduction;
        }

        public String getId(){return id;}
        public String getName() {
            return name;
        }

        public String getStalk() {
            return stalk;
        }
        public String getIntroduction() {
            return introduction;
        }

    }
}
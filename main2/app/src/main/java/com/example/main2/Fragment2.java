package com.example.main2;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.main2.databinding.Fragment2Binding;
import com.example.main2.databinding.Fragment4Binding;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Fragment2 extends Fragment {

    private Fragment2Binding binding2;
    List<String> list2;
    List<Integer> imgList = Arrays.asList(R.drawable.album1, R.drawable.album2, R.drawable.album3, R.drawable.album4,
            R.drawable.album5, R.drawable.album6, R.drawable.album7, R.drawable.album8, R.drawable.album9,
            R.drawable.album10, R.drawable.album11, R.drawable.album12, R.drawable.album13, R.drawable.album14);

    Context context2;
    RecyclerView recyclerView2;
    MyAdapter2 myadapter2;

    private int[] image = {R.drawable.album1, R.drawable.album2, R.drawable.album3, R.drawable.album4};

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        binding2 = Fragment2Binding.inflate(inflater,container,false);
        View view = binding2.getRoot();
        context2 = view.getContext();


        recyclerView2=binding2.recyclerView2;
        list2 = new ArrayList<>();
        initData();


        LinearLayoutManager manager=new LinearLayoutManager(context2);
        recyclerView2.setLayoutManager(manager);

        // 设置为横向布局
        manager.setOrientation(LinearLayoutManager.HORIZONTAL);


//        DividerItemDecoration的实现类用于实现分割线
//        我们需要自己实现ItemDecoration接口
        DividerItemDecoration dividerItemDecoration = new DividerItemDecoration(context2,DividerItemDecoration.HORIZONTAL);
        recyclerView2.addItemDecoration(dividerItemDecoration);

//        新建一个RecyclerView的适配器，并传入数据
        myadapter2 = new MyAdapter2(list2,imgList, context2);
        recyclerView2.setAdapter(myadapter2);


        return  view;
    }

    public void initData() {
        list2.add("OP《ヒャダインのカカカタ☆カタオモイ-C》");
        list2.add("OP2《ヒャダインのじょーじょーゆーじょー》");
        list2.add("ED《Zzz》");
        list2.add("ED集《日常の合唱曲》");
        list2.add("《なののネジ回しラプソディ》");
        list2.add("《はかせの好きなのなの》");
        list2.add("《阪本さんのニャーというとでも思ったか》");
        list2.add("《みおのカプってカプって萌えちぎれ》");
        list2.add("《ゆっこのスラマッパギだよ人生は》");
        list2.add("《麻衣の涙の阿弥陀如来》");
        list2.add("《笹原のコジロウポルカ》");
        list2.add("《みさとの学校生活は爆発だっ!!》");
        list2.add("《キャラクターミニアルバム》");
        list2.add("《麻衣ペース》");
    }

    public void onDestroyView(){
        super.onDestroyView();
        binding2 = null;
    }


}
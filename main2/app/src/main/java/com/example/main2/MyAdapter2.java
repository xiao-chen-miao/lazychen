package com.example.main2;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;
import java.util.Map;

public class MyAdapter2 extends RecyclerView.Adapter<MyAdapter2.MyHolder2> {
    Context context2;
    List<String> list2;

    List<Integer> image;

    public MyAdapter2(List<String> list, List<Integer> image, Context context){
        this.list2 = list;
        this.image = image;
        this.context2 = context;
    }



    @NonNull
    @Override
    public MyHolder2 onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        View view = new View(context2);
        view = LayoutInflater.from(context2).inflate(R.layout.item2, parent,false);

        MyHolder2 myHolder2 = new MyHolder2(view);
        return  myHolder2;
    }

    @Override
    public void onBindViewHolder(@NonNull MyHolder2 holder2, int position) {
        holder2.albumView.setText(list2.get(position));
        holder2.imageView.setImageResource(image.get(position));
    }

    @Override
    public int getItemCount() {
        return list2.size();
    }


    class MyHolder2 extends RecyclerView.ViewHolder{
        TextView albumView;
        ImageView imageView;
        public MyHolder2(@NonNull View itemView) {
            super(itemView);
            Log.d("cjj", "MyHolder2");
            albumView = itemView.findViewById(R.id.albumView);
            imageView = itemView.findViewById(R.id.albumImageView);
        }
    }

}

package com.example.main2;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class MyAdapter extends RecyclerView.Adapter<MyAdapter.MyHolder> {

    List<Fragment1.Figures> list;
    List<Integer> imgList;
    Context context;

    public MyAdapter(List<Fragment1.Figures> list, List<Integer> imgList, Context context) {
        this.list = list;
        this.imgList=imgList;
        this.context = context;
    }

    @NonNull
    @Override
    public MyHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
//        新建hodler，并送出hodler
        View view = new View(context);
        view = LayoutInflater.from(context).inflate(R.layout.item, parent,false);

        MyHolder myholder = new MyHolder(view);
        return myholder;
    }

    @Override
    public void onBindViewHolder(@NonNull MyHolder holder, int position) {
        Fragment1.Figures figure = list.get(position);
        holder.textView1.setText(figure.getName());
        holder.layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
//                点击跳转人物详情页面,使用Intent实现
                Intent intent = new Intent(view.getContext(), FigureActivity.class);
//                传递人物id，用于查找人物图片
                intent.putExtra("id", figure.getId());
//                传递人物姓名,梗语句和简介
                intent.putExtra("name", figure.getName());
                intent.putExtra("stalk", figure.getStalk());
                intent.putExtra("introduction", figure.getIntroduction());
//                启动Intent
                view.getContext().startActivity(intent);
            }
        });
    }

    @Override
    public int getItemCount() {
        return list.size();
    }
    class MyHolder extends RecyclerView.ViewHolder{
        TextView textView1;
        LinearLayout layout;
        public MyHolder(@NonNull View itemView) {
            super(itemView);
            textView1 = itemView.findViewById(R.id.textView21);
            layout = itemView.findViewById(R.id.layout1);
        }
    }





}

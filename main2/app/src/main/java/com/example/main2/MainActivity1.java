package com.example.main2;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import android.os.Bundle;
import android.view.View;
import com.example.main2.databinding.ActivityMain1Binding;
import com.example.main2.databinding.Fragment1Binding;


public class MainActivity1 extends AppCompatActivity implements View.OnClickListener {


    private ActivityMain1Binding binding;

    Fragment1 fragment1;
    Fragment  fragment2,fragment3,fragment4;

    FragmentManager fm;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = ActivityMain1Binding.inflate(getLayoutInflater());
        View view = binding.getRoot();
        setContentView(view);


        fm = getSupportFragmentManager();
        fragment1 = new Fragment1();
        fragment2 = new Fragment2();
        fragment3 = new Fragment3();
        fragment4 = new Fragment4();

        fragmentHide();
        innitial();

        binding.bottom.linearlayout1.setOnClickListener(this);
        binding.bottom.linearlayout2.setOnClickListener(this);
        binding.bottom.linearlayout3.setOnClickListener(this);
        binding.bottom.linearlayout4.setOnClickListener(this);

    }

    private void fragmentHide() {
        FragmentTransaction ft = fm.beginTransaction()
                .hide(fragment1)
                .hide(fragment2)
                .hide(fragment3)
                .hide(fragment4);
        ft.commit();
    }

    private void innitial(){
        FragmentTransaction ft = fm.beginTransaction()
                .add(R.id.content, fragment1)
                .add(R.id.content, fragment2)
                .add(R.id.content, fragment3)
                .add(R.id.content, fragment4);
        ft.commit();

    }

    private void fragmentshow(Fragment fragment) {
        fragmentHide();
        FragmentTransaction ft = fm.beginTransaction()
                .show(fragment);
        ft.commit();
    }

    @Override
    public void onClick(View view) {

        int id = view.getId();
        if (id == binding.bottom.linearlayout1.getId()){
            fragmentshow(fragment1);
        } else if (id == binding.bottom.linearlayout2.getId()) {
            fragmentshow(fragment2);
        } else if (id == binding.bottom.linearlayout3.getId()) {
            fragmentshow(fragment3);
        }else{
            fragmentshow(fragment4);
        }
    }
    }

package com.example.main2;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.widget.ImageView;
import android.widget.TextView;

import java.lang.reflect.Field;

public class FigureActivity extends AppCompatActivity {

    private TextView nameView;
    private TextView stalkView;
    private TextView introView;
    private ImageView imageView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_figure);



//        绑定View
        nameView = findViewById(R.id.nameView);
//        设置文字粗体显示
        nameView.setTypeface(Typeface.defaultFromStyle(Typeface.BOLD));
        stalkView = findViewById(R.id.stalkView);
        introView = findViewById(R.id.introView);
        imageView = findViewById(R.id.figureView);


        Intent intent = getIntent();
        String id = intent.getStringExtra("id");
        String name = intent.getStringExtra("name");
        String stalk = intent.getStringExtra("stalk");
        String introduction = intent.getStringExtra("introduction");

//        遍历drawable的所有图片，找到我们想要的图片的id
        int resId = 0;
        Field[] fields = R.drawable.class.getFields();
        for (Field field : fields) {
            try {
                if (field.getType() == int.class) {
                    String fieldName = field.getName();
                    if (fieldName.equals("figure" + id)) {
                        resId = field.getInt(null);
                    }
                }
            } catch (IllegalAccessException e) {
                throw new RuntimeException(e);
            }

//            为View提供数据
            nameView.setText(name);
            stalkView.setText(stalk);
            introView.setText(introduction);
            imageView.setImageResource(resId);

        }
    }
}
package com.example.ocractivity;

import androidx.activity.result.ActivityResult;
import androidx.activity.result.ActivityResultCallback;
import androidx.activity.result.ActivityResultLauncher;
import androidx.activity.result.contract.ActivityResultContracts;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.FileProvider;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.Message;
import android.provider.MediaStore;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;


public class MainActivity extends AppCompatActivity{
    private Button btn_scan;
    public static final int TAKE_PHOTO = 1;// 系统相机

    private Uri Imageuri;
    private File outputImage;
    private IdentifyResult result;
    private TextView textView;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        context=this;
        btn_scan = (Button) findViewById(R.id.button);
//        picture = (ImageView) findViewById(R.id.picture);
        textView = (TextView) findViewById(R.id.textView);
        btn_scan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // 创建File对象，用于存储拍照后的图片
                outputImage = new File(getExternalCacheDir(), "img.jpg");
                try {
                    if (outputImage.exists()) {
                        outputImage.delete();
                    }
                    outputImage.createNewFile();
                } catch (IOException e) {
                    e.printStackTrace();
                }
                // 启用相机
                if (Build.VERSION.SDK_INT>=24){
                    Imageuri= FileProvider.getUriForFile(MainActivity.this,
                            "com.example.myapplication.fileprovider",outputImage);
                }else {
                    Imageuri=Uri.fromFile(outputImage);
                }
                Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);//捕获图片
                intent.putExtra(MediaStore.EXTRA_OUTPUT,Imageuri);
                launcher.launch(intent);
            }
        });
    }
    public ActivityResultLauncher<Intent> launcher = registerForActivityResult(
            new ActivityResultContracts.StartActivityForResult(),
            new ActivityResultCallback<ActivityResult>(){
                @Override
                public void onActivityResult(ActivityResult result) {
                    if (result.getResultCode() == Activity.RESULT_OK){
                        //使用身份证识别接口获取
                        IdCardDetails(getHandler_IdCard);

                    }
                }
            });

    Handler getHandler_IdCard = new Handler(){
        @Override
        public void handleMessage(@NonNull Message msg) {
            super.handleMessage(msg);
            String str=msg.getData().getString("request");
            JSONObject jsonObject=null;
            try {
                jsonObject=new JSONObject(str);
            }catch (JSONException e){
                e.printStackTrace();
            }
            try {
                result=new Gson().fromJson(jsonObject.get("Response").toString(),IdentifyResult.class);
                textView.setText(getString(result));
            }catch (JSONException e){
                e.printStackTrace();
            }
        }
    };



    /**
     * 身份证扫描结果
     */
    public void IdCardDetails(final Handler handler){
        String str = null;
        if (outputImage.exists()) {
            str = outputImage.toString();
            toast("找到图片:"+str);
        } else {
            toast("找不到图片");
            return;
        }
        //利用腾讯云V3签名算法封装图片，将封装后的数据上传至腾讯云服务器，再获得服务器返回数据
        TecentHttpUtil.getIdCardDetails(str,
                new TecentHttpUtil.SimpleCallBack() {

                    @Override
                    public void Succ(String response) {
                        Message message=new Message();
                        Bundle bundle=new Bundle();
                        bundle.putString("request",response);
                        message.setData(bundle);
                        handler.sendMessage(message);
                        toast("识别成功！");
                    }
                    @Override
                    public void error() {
                        toast("识别错误！");
                    }
                });
    }

    public String getString(IdentifyResult result) {
        StringBuilder sb = new StringBuilder();
        sb.append("姓名: ").append(result.getName()).append("\n");
        sb.append("性别: ").append(result.getSex()).append("\n");
        sb.append("民族: ").append(result.getNation()).append("\n");
        sb.append("地址: ").append(result.getAddress()).append("\n");
        Log.d("scc",sb.toString());
        return sb.toString();
    }

    public void toast(String str) {
        Toast.makeText(getApplicationContext(), str, Toast.LENGTH_SHORT).show();
    }

}